pragma solidity ^0.6.4;

contract Persist {
    struct Complaint{
        bytes key;
        bytes validation;
    }

    Complaint[] public complaints;
    uint public next;

    function createComplaint(bytes memory _validation,bytes memory _key) public{
        complaints.push(Complaint(_key,_validation));
        next ++;
    }
    function getValidation(uint _id) public view returns(bytes memory,bytes memory){
        return (complaints[_id].key,complaints[_id].validation);
    }
    function updateValidation(uint _id,bytes memory _validation) public{
        complaints[_id].validation = _validation;
    }
    function getIndex() public view returns(uint){
        return(next);
    }
}