const Web3 = require("web3");
const decoder = require('abi-decoder');
const persistArtifact = require('../../build/contracts/Persist.json');

let web3 = new Web3( "ws://localhost:7545");

const val = 'juan';

let deployedNetwork,contract,account;

async function start(){
    try {
        const networkId = await web3.eth.net.getId();
        deployedNetwork = persistArtifact.networks[networkId];
        decoder.addABI(persistArtifact.abi);
        contract = new web3.eth.Contract(
            persistArtifact.abi,
            deployedNetwork.address
        );
        account = await web3.eth.getAccounts();
        account = account[0];
        //createComplaint(val,'bien')
        //getTransaction();
        //getIndex();
        getComplaint(0);
        //console.log(await updateComplaint(0,'ACTIVE'));
    } catch (error) {
        console.log(error);
    }
}
async function createComplaint(key,hash){
    const id = await contract.methods.createComplaint(web3.utils.asciiToHex(hash),web3.utils.asciiToHex(key)).send({gas: 140000, from: account});
    console.log('id->',id);
    console.log(id.receipt);
    console.log(id.receipt.events);
}
async function getComplaint(id){
    try {
        const data = await contract.methods.getValidation(id).call();
        console.log(data)
        console.log(web3.utils.hexToAscii(data['0']),web3.utils.hexToAscii(data['0']))
    } catch (error) {
        console.log(error);
    }
}
async function getIndex(){
    try {
        const data = await contract.methods.getIndex().call();
        console.log('index->',data);
    } catch (error) {
        console.log(error)
    }
}
async function getTransaction(trxid){
    try {
        const data = await web3.eth.getTransaction('0x3b9ff9230d2a060403c8b45311469506c9925048dd51b13083c952db09c9ed16');
        console.log(data);
        const decoded = decoder.decodeMethod(data.value);
        console.log('decoded->',decoded);
        const validation = web3.utils.hexToAscii(decoded.params[0].value);
        console.log('value->',validation);
        return;
    } catch (error) {
        console.log(error);
    }
}
async function updateComplaint(id,hash){
    try {
        const data = await contract.methods.updateValidation(id,web3.utils.asciiToHex(hash)).send({gas: 140000, from: account});
        return {
            trx:data.transactionHash
        }
    } catch (error) {
        console.log('web3',error);
        throw ({EXCEPTION:'GENERICEXCEPTION',CODE:error});
    }
}
start();