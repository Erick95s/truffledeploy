let data = new web3.eth.Contract(abi, credentialChainContractAddress)
.methods.upsertTicket(ticket.officeTicketAddres,ticket.office_id, ticket.client_id, ticket.client_id, ticket.idTicket, ticket.ticket_number,ticket.metadata,existsRecord,nonce,signature).encodeABI()
let gasLimit = await new web3.eth.Contract(abi, credentialChainContractAddress)
.methods.upsertTicket(ticket.officeTicketAddres,ticket.office_id, ticket.client_id,ticket.client_id,ticket.idTicket, ticket.ticket_number,ticket.metadata,existsRecord,nonce,signature).estimateGas({from: credentialChainAddress})
let gasPrice = await web3.eth.getGasPrice() //Wei
let txHash = await Web3Connector.create(data,gasLimit,gasPrice).catch((e) =>{console.log("Error tx::",e)})
let ehtTotal =gasPrice*gasLimit
let etherValue = Web3.utils.fromWei(ehtTotal.toString(), 'ether');
logger.write("\n"+ moment().format()  + text + '- GasLimit: ' + gasLimit.toString() + ' GasPrice: ' + gasPrice.toString())
logger.end()
result = {
    officeTicketAddres_id: ticket.officeTicketAddres,
    clientIdAddress: ticket.client_id,
    transaction: txHash,
    eth_cost: parseFloat(etherValue.toString())
};